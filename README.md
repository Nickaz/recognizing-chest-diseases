# Django Project

Works with PostgreSQL.

## Install requirements

```shell
$ pip install -r requirements/development.txt
```

## Run a database

```shell
$ docker-compose up -d postgres
```

## Set up local settings

```shell
$ vi config/settings/local.py
```

Put your local settings in the `local.py`, you can override settings
consider your local environment. Start with lines below:

```python
from .base import *
```


## Apply migrations

```shell
$ ./manage.py migrate
```


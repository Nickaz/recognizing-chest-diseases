from rest_framework import serializers
from .models import Patient, Radiograph

from drf_writable_nested.serializers import WritableNestedModelSerializer

class PatientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Patient
        fields = (
            'id',
            'first_name',
            'last_name',
            'birth_date',
        )
        read_only_fields = (
            'id',
        )
        
        
class RadiographSerializer(WritableNestedModelSerializer):
    
    patient = PatientSerializer()
    
    class Meta:
        model = Radiograph
        fields = (
            'patient',
            'snapshot',
            'diagnosis',
            'date',
        )
        read_only_fields = (
            'diagnosis',
        )

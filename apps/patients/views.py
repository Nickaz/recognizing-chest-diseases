from django.views.generic import TemplateView, ListView, CreateView
from rest_framework import generics
from rest_framework.viewsets import ModelViewSet

from .serializers import RadiographSerializer, PatientSerializer
from .models import Radiograph, Patient
from .forms import PatientModelForm, RadiographModelForm


class MainPageView(TemplateView):
    template_name = 'main_page.html'


class PatientsList(ListView):
    model = Patient
    template_name = 'patients.html'
    
    
class PatientCreate(CreateView):
    model = Patient
    fields = (
        'first_name',
        'last_name',
        'birth_date',
    )
    template_name = "new_patient.html"
    success_url = '/patients'


class RadiographsList(ListView):
    model = Radiograph
    template_name = 'radiographs.html'


class RadiographCreate(CreateView):
    model = Radiograph
    form_class = RadiographModelForm
    template_name = 'new_radiograph.html'
    success_url = '/'

class PatientViewSet(ModelViewSet):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer


class RadiographViewSet(ModelViewSet):
    queryset = Radiograph.objects.all()
    serializer_class = RadiographSerializer

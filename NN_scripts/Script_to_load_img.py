from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.preprocessing import image

def max2(z):
    s = 0
    b = 0
    for x in range(len(z)):
        if z[x] > s:
            s = z[x]
            b = x
    return b

classes = ["atelectasis", "cardiomegaly", "effusion", "infiltrate", "mass", "nodule", "pneumonia"]

model = load_model("chest_x1.h5")

model.compile(loss="categorical_crossentropy",
                     optimizer="adam",
                     metrics=["accuracy"])

img = image.load_img("E:/PYTHON/CHEST_X/test/atelectasis/00007557_026.png", target_size=(900, 900))

x = image.img_to_array(img)
x = x.reshape(-1, 900, 900, 3)
x /= 255

prediction = model.predict(x)

print(classes[max2(prediction[0])])
print(prediction[0])
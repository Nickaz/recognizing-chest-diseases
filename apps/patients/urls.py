from django.urls import path, include
from rest_framework import routers

from .views import (
    RadiographViewSet,
    PatientViewSet,
    MainPageView,
    PatientsList,
    PatientCreate,
    RadiographsList,
    RadiographCreate
)

router = routers.SimpleRouter()
router.register('radiographs', RadiographViewSet)
router.register('patients', PatientViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('', MainPageView.as_view(), name='main_page'),
    path('patients', PatientsList.as_view(), name='patients'),
    path('parients/add', PatientCreate.as_view(), name='add_patient'),
    path('radiographs', RadiographsList.as_view(), name='radiographs'),
    path('radiographs/add', RadiographCreate.as_view(), name='add_radiographs'),
]

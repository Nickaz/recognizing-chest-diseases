from django.db import models

from .services import image_path


class Patient(models.Model):
    first_name = models.TextField()
    last_name = models.TextField()
    birth_date = models.DateField()
    registered = models.DateTimeField(
        auto_now_add=True
    )
    
    def __str__(self):
        return f'{self.first_name} {self.last_name} {self.birth_date}'

class Radiograph(models.Model):
    snapshot = models.ImageField(
        upload_to=image_path,
    )
    diagnosis = models.TextField()
    date = models.DateField()
    patient = models.ForeignKey(
        to=Patient,
        on_delete=models.CASCADE,
        related_name='radiographs',
        null=True
    )
    
import os
import pandas as pd
import shutil
from PIL import Image as mg
import matplotlib.pyplot as plt

def create_directory(dir_name):
    if os.path.exists(dir_name):
        shutil.rmtree(dir_name)
    os.makedirs(dir_name)
    os.makedirs(os.path.join(dir_name, "effusion"))
    os.makedirs(os.path.join(dir_name, "infiltrate"))
    os.makedirs(os.path.join(dir_name, "mass"))
    os.makedirs(os.path.join(dir_name, "atelectasis"))
    os.makedirs(os.path.join(dir_name, "pneumonia"))
    os.makedirs(os.path.join(dir_name, "pneumotrax"))
    os.makedirs(os.path.join(dir_name, "nodule"))
    os.makedirs(os.path.join(dir_name, "cardiomegaly"))

def create_list(ailment, list):
    new_list = []
    for i in range(len(list) - 1):
        if list[i][0] == ailment:
            new_list.append(list[i][1])
    return new_list

def copy_images(image_list, src_dir, dest_dir, name):
    for image in image_list:
        if os.path.exists("E:/PYTHON/CHEST_X" + src_dir + image):
            shutil.copy2("E:/PYTHON/CHEST_X" + src_dir + image, "E:/PYTHON/CHEST_X/" + dest_dir + name)

def to_copy(list, percent, name, data_dir, test_dir, val_dir, train_dir):
    copy_images(list[:int(percent * len(list))], data_dir, test_dir, name)
    copy_images(list[int(-percent * len(list)):], data_dir, val_dir, name)
    copy_images(list[int(percent * len(list)): int(-percent * len(list)) - 1], data_dir,
                train_dir, name)


folder = os.listdir(path = "E:/torrents/ChestXray-NIHCC")
bbox = pd.read_csv('E:/torrents/ChestXray-NIHCC/BBox_List_2017.csv', delimiter= ',')

diagnoses = bbox.loc[:, 'Finding Label']
images = bbox.loc[:, 'Image Index']

#direc = os.open(path="E:/PYTHON/CHEST_X/images_to_train")

data_dir = "/images_to_train/images/"

train_dir = "train/"
val_dir = "val/"
test_dir = "test/"

percent = 0.15

list_of_ailments = bbox['Finding Label']
list_of_images = bbox['Image Index']

list_of_everything = []

for i in range(len(list_of_ailments) - 1):
    list_of_everything.append([list_of_ailments[i], list_of_images[i]])


create_directory(train_dir)
create_directory(val_dir)
create_directory(test_dir)

list_atel = create_list("Atelectasis", list_of_everything)
list_efus = create_list("Effusion", list_of_everything)
list_infil = create_list("Infiltrate", list_of_everything)
list_mass = create_list("Mass", list_of_everything)
list_pneumon = create_list("Pneumonia", list_of_everything)
list_pneumot = create_list("Pneumotrax", list_of_everything)
list_nod = create_list("Nodule", list_of_everything)
list_cardio = create_list("Cardiomegaly", list_of_everything)

to_copy(list_atel, percent, "atelectasis", data_dir, test_dir, val_dir, train_dir)
to_copy(list_efus, percent, "effusion", data_dir, test_dir, val_dir, train_dir)
to_copy(list_infil, percent, "infiltrate", data_dir, test_dir, val_dir, train_dir)
to_copy(list_mass, percent, "mass", data_dir, test_dir, val_dir, train_dir)
to_copy(list_pneumon, percent, "pneumonia", data_dir, test_dir, val_dir, train_dir)
to_copy(list_pneumot, percent, "pneumotrax", data_dir, test_dir, val_dir, train_dir)
to_copy(list_nod, percent, "nodule", data_dir, test_dir, val_dir, train_dir)
to_copy(list_cardio, percent, "cardiomegaly", data_dir, test_dir, val_dir, train_dir)

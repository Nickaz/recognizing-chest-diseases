from django.forms import ModelForm

from .models import Patient, Radiograph


class PatientModelForm(ModelForm):
    
    class Meta:
        model = Patient
        fields = (
            'first_name',
            'last_name',
            'birth_date',
        )
        
        
class RadiographModelForm(ModelForm):
    
    class Meta:
        model = Radiograph
        fields = (
            'patient',
            'snapshot',
            'date',
        )

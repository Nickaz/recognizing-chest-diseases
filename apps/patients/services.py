import pathlib
import uuid


def image_path(instance, filename):
    """Get unique filename with uuid.

    File will be uploaded to
    MEDIA_ROOT/patients/radiographs/unique_id.{<filename> extension}
    """
    file = pathlib.Path(filename)
    return f'patients/radiographs/{uuid.uuid4()}{file.suffix}'

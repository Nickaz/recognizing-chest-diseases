import os
import tarfile as tr
import pandas as pd
import shutil
from PIL import Image as mg
import matplotlib.pyplot as plt


folder = os.listdir(path = "E:/torrents/ChestXray-NIHCC")
bbox = pd.read_csv('E:/torrents/ChestXray-NIHCC/BBox_List_2017.csv', delimiter= ',')

diagnoses = bbox.loc[:, 'Finding Label']
images = bbox.loc[:, 'Image Index']

#direc = os.open(path="E:/PYTHON/CHEST_X/images_to_train")

set_res = set(diagnoses)
print("The unique elements of the input list using set():n")
list_res = (list(set_res))


for i in range(1, 13):
    if i < 10:
        tar = tr.open(("E:/torrents/ChestXray-NIHCC/images_00{0}.tar.gz").format(i), "r")
    elif 10 <= i <= 12:
        tar = tr.open(("E:/torrents/ChestXray-NIHCC/images_0{0}.tar.gz").format(i), "r")
    for file in tar:
        for str in images:
            if file.name.find(str) != -1:
                tar.extract(file.name, "E:/PYTHON/CHEST_X/images_to_train")
    tar.close()
    print(("Done with archive{0}").format(i))
# Generated by Django 3.1.2 on 2020-11-01 10:59

import apps.patients.services
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Radiograph',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('snapshot', models.ImageField(upload_to=apps.patients.services.image_path)),
                ('diagnosis', models.TextField()),
                ('date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.TextField()),
                ('last_name', models.TextField()),
                ('birth_date', models.DateField()),
                ('registered', models.DateTimeField(auto_now_add=True)),
                ('radiographs', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='patient', to='patients.radiograph')),
            ],
        ),
    ]
